Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ckermit
Upstream-Contact: Frank da Cruz <fdc@columbia.edu>
Source: http://www.kermitproject.org/ck90.html

Files: *
Copyright: 1985-2022 Trustees of Columbia University in the City of New York
           2002-2005 Secure Endpoints Inc, New York NY USA
           1995 Oy Online Solutions Ltd., Jyvaskyla, Finland
           1990 Massachusetts Institute of Technology
           1991, 1993 Regents of the University of California
           1991, 1992, 1993, 1994, 1995 AT&T
           1995, 1997 Eric Young <eay@cryptosoft.com>
           1997 Stanford University
License: BSD-3-clause

Files: debian/*
Copyright: 2003-2012 Ian Beckwith <ianb@debian.org>
           2020-2022 Sébastien Villemot <sebastien@debian.org>
License: BSD-3-clause

License: BSD-3-clause
 THE C-KERMIT 9.0 LICENSE
 .
 Fri Jun 24 14:43:35 2011
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
  + Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
  + Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
  + Neither the name of Columbia University nor the names of any
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
