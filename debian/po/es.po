# ckermit po-debconf translation to Spanish
# Copyright (C) 2009 Software in the Public Interest
# This file is distributed under the same license as the ckermit package.
#
# Changes:
#   - Initial translation
#       Francisco Javier Cuadrado <fcocuadrado@gmail.com>, 2009
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas y normas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: ckermit 211-13\n"
"Report-Msgid-Bugs-To: ckermit@packages.debian.org\n"
"POT-Creation-Date: 2020-09-17 18:07+0200\n"
"PO-Revision-Date: 2020-09-17 18:09+0200\n"
"Last-Translator: Francisco Javier Cuadrado <fcocuadrado@gmail.com>\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Enable Internet Kermit Service Daemon (IKSD) in inetd.conf?"
msgstr ""
"¿Desea activar el demonio del servicio Kermit de internet (IKSD) en el "
"archivo «inetd.conf»?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"The Internet Kermit Service Daemon (IKSD) is the C-Kermit program running as "
"an Internet service, similar to an FTP or Telnet server.  It executes Telnet "
"protocol just like a Telnet server and it transfers files like an FTP "
"server.  But unlike an FTP server, IKSD uses the Kermit file transfer "
"protocol (which is more powerful and flexible) and allows both FTP-like "
"client/server connections as well as direct keyboard interaction.  Secure "
"authentication methods and encrypted sessions are available, as well as a "
"wide range of file transfer and management functions, which can be scripted "
"to automate arbitrarily complex tasks."
msgstr ""
"El demonio del servicio Kermit de internet (IKSD) es el programa C-Kermit "
"ejecutándose como un servicio de internet, similar a un servidor de FTP o de "
"Telnet. Ejecuta el protocolo de Telnet como si fuera un servidor de Telnet y "
"transfiere los archivos como un servidor de FTP. Pero a diferencia de un "
"servidor FTP, IKSD utiliza el protocolo de transferencia de archivos de "
"Kermit (que es más potente y flexible) y, permite conexiones con el cliente "
"y el servidor similares a FTP así como interacción directa desde el teclado. "
"Están disponibles los métodos de autenticación seguros y las sesiones "
"cifradas, así como un amplio rango de funciones de transferencia y gestión "
"de archivos, que se pueden automatizar mediante scripts para ejecutar tareas "
"más complejas."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Enable anonymous IKSD logins?"
msgstr "¿Desea activar el acceso anónimo en IKSD?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"IKSD supports anonymous logins (using chroot), similar to anonymous FTP."
msgstr ""
"IKSD permite acceder de forma anónima (utilizando chroot), de forma parecida "
"a un FTP anónimo."

#. Type: string
#. Description
#: ../templates:3001
msgid "Directory for anonymous IKSD logins:"
msgstr "Directorio para los accesos anónimos de IKSD:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Enter directory for anonymous IKSD logins. A chroot() will be performed into "
"this directory on login. This directory will NOT be created."
msgstr ""
"Introduzca el directorio para los accesos anónimos de IKSD. Se realizará un "
"chroot() en este directorio al identificarse. NO se creará este directorio."

#. Type: string
#. Description
#: ../templates:3001
msgid "The default is /srv/ftp (same as proftpd)."
msgstr "El directorio predeterminado es «/srv/ftp» (al igual que en proftpd)."

#. Type: error
#. Description
#: ../templates:4001
msgid "No inet daemon found, so IKSD cannot be configured."
msgstr ""
"No se ha encontrado el demonio inet, de modo que no se puede configurar IKSD."

#. Type: error
#. Description
#: ../templates:4001
msgid ""
"Please install an inetd (e.g. openbsd-inetd) and then reconfigure ckermit "
"with:"
msgstr ""
"Instale un demonio de inet (por ejemplo: openbsd-inetd) y reconfigure "
"ckermit ejecutando:"

#~ msgid "dpkg-reconfigure ckermit"
#~ msgstr "dpkg-reconfigure ckermit"
